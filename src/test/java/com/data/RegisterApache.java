package com.data;

import java.io.File;

import java.io.FileInputStream;
import java.io.IOException;
import java.time.Duration;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class RegisterApache {

	public static void main(String[] args) throws IOException {
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		driver.get("https://demowebshop.tricentis.com/");
		File f = new File("/home/reshma/Documents/datadriven/testdata2.xlsx");
		FileInputStream fn=new FileInputStream(f);
		XSSFWorkbook workbook = new XSSFWorkbook(fn);
		XSSFSheet sh = workbook.getSheetAt(1);
		int rows = sh.getPhysicalNumberOfRows();
		for (int i = 1; i < rows; i++) {
			String Firstname = sh.getRow(i).getCell(0).getStringCellValue();
			String lastname = sh.getRow(i).getCell(1).getStringCellValue();
			String email = sh.getRow(i).getCell(2).getStringCellValue();
			String password = sh.getRow(i).getCell(3).getStringCellValue();
			String gender = sh.getRow(i).getCell(4).getStringCellValue();


			driver.findElement(By.linkText("Register")).click();
			
				if (gender.equalsIgnoreCase("male")) {
					driver.findElement(By.id("gender-male")).click();

				}else {
					driver.findElement(By.id("gender-female")).click();
				}
			driver.findElement(By.id("FirstName")).sendKeys(Firstname);
			driver.findElement(By.id("LastName")).sendKeys(lastname);
			driver.findElement(By.id("Email")).sendKeys(email);
			driver.findElement(By.id("Password")).sendKeys(password);
			driver.findElement(By.id("ConfirmPassword")).sendKeys(password);
			driver.findElement(By.id("register-button")).click();
		}

		



	}

}
