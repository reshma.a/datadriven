package com.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class RegisterData {

	public static void main(String[] args) throws BiffException, IOException {
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		driver.get("https://demowebshop.tricentis.com/");
		File f= new File("/home/reshma/Documents/datadriven/testdata.xls");

		FileInputStream fi=new FileInputStream(f);
		Workbook book = Workbook.getWorkbook(fi);
		Sheet sh= book.getSheet("registerdata");

		int rows=sh.getRows();
		int columns = sh.getColumns();
		
		for (int i = 1; i < rows; i++) {
			String firstname = sh.getCell(0,i).getContents();
			String lastname = sh.getCell(1,i).getContents();
			String email = sh.getCell(2,i).getContents();
			String pasword = sh.getCell(2,i).getContents();
			
			driver.findElement(By.linkText("Register")).click();
			driver.findElement(By.id("gender-female")).click();
			driver.findElement(By.id("FirstName")).sendKeys(firstname);
			driver.findElement(By.id("LastName")).sendKeys(lastname);
			driver.findElement(By.id("Email")).sendKeys(email);
			driver.findElement(By.id("Password")).sendKeys(pasword);
			driver.findElement(By.id("ConfirmPassword")).sendKeys(pasword);
			driver.findElement(By.id("register-button")).click();
		}
		
		
	}

}
